Feature: Add the cheapest iphone from Amazon to basket

  Scenario: Search for iphone, add it to basket and then add an accessory to the basket
    Given I am on Amazon's homepage
    And I search for the term 'iPhone'
    Then I should see some results
    When I refine results by 'Apple'
    And I click link 'Mobile Phones & Smartphones'
    And I sort the Price in ascending order
    And I select the lowest priced item
    Then I am taken to the product page
    When I add the item to my basket
    Then I see confirmation that my item has been added
    And the basket total price is correct
    And I search for the term 'iPhone case'
    Then I should see some results
    When I refine results by 'Apple'
    And I click link 'Mobile Phone Cases & Covers'
    And I filter the item by review rank
    And I select the lowest priced item
    Then I am taken to the product page
    When I add the item to my basket
    Then I see confirmation that my item has been added
    And the basket price is updated correctly


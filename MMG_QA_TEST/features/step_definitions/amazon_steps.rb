Given(/^I am on Amazon's homepage$/) do
  homepage.load
end

Given(/^I search for the term '([^"]*)'$/) do |search_term|
  homepage.header.search_input.set search_term
  homepage.header.search_input.send_keys(:enter)
end

Then(/^I should see some results$/) do
  expect(results_page).to be_displayed
  expect(results_page).to have_results
end

When(/^I refine results by '([^"]*)'$/) do |refinement|
  results_page.refinements.select(refinement)
end

When(/^I sort the Price in ascending order$/) do
  results_page.sort.select('Price: Low to High')
end

When(/^I click link '([^"]*)'$/) do |link_title|
  first(:link, link_title).click
  wait_until do
    results_page.has_results?
  end
end

When(/^I select the lowest priced item/) do
  @price = results_page.results.items.first.price.text
  @details = results_page.results.items.first.details.text
  puts "Item selected:"+ @details
  results_page.results.items.first.image.click
end


Then(/^I am taken to the product page$/) do
  product_page.current_url
  @prodtitle = product_page.title.text
  expect(product_page.current_url).to include "iPhone"
  @prodprice = product_page.prodprice.text
  if @price == @prodprice
    then "Item matches user selection"
  end
  @actualprodprice = @prodprice
  @actualprodprice.gsub!(/£/,'')
  @actualprodprice.to_f
end

Then(/^I add the item to my basket$/) do
   product_page.addtobasket.click
end

Then(/^I see confirmation that my item has been added$/) do
  expect(confirmation_page.addedtobasket.text).to eq "Added to Basket"
  if confirmation_page.addedtobasket.has_text?
    then puts "Item has successfully been added to basket"
  end
end

And(/^the basket total price is correct$/) do
   @basketprice = confirmation_page.basketconfirmation.basketprice.text
   @runningtotal = @basketprice
   @runningtotal.gsub!(/£/,'')
   @runningtotal.to_f
   if @basketprice == @prodprice
   then puts "Current basket total is £" + @basketprice
   end
end

Then(/^the basket price is updated correctly$/) do
  @basketprice = confirmation_page.basketconfirmation.basketprice.text
  @basketprice.gsub!(/£/,'')
  if @basketprice.to_f == @runningtotal.to_f + @actualprodprice.to_f
  puts "The total basket price is £" + @basketprice
end
end

When(/^I filter the item by review rank$/) do
  results_page.sort.select('Avg. Customer Review')
end


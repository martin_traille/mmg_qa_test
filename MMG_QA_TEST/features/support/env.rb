require 'bundler'
require 'capybara'
require 'capybara/dsl'
require 'site_prism'
PROJECT_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '..', '..'))
$LOAD_PATH << File.join(PROJECT_ROOT, 'lib')

Bundler.require

require_all 'lib'
chrome_caps = { browser: 'chrome'.to_sym }
Capybara.register_driver :selenium do |app|
  Capybara::Selenium::Driver.new(app, chrome_caps)
end

Capybara.configure do |config|
  config.run_server = false
  config.default_driver = :selenium
  # Setting this to the quickest wait to avoid stalling
  # on pages that are not slow. Use
  #
  #   using_slow_execution_block do
  #   end
  #
  # between code blocks that are slow responding instead
  config.default_max_wait_time = 10
  config.app_host = "https://www.amazon.co.uk/"
end

SitePrism.configure do |config|
  config.use_implicit_waits = true
end

World(Capybara::DSL)

def wait_until
  Timeout.timeout(10) do
    sleep(0.1) until value = yield
    value
  end
end

module Pages
  class BasePage < SitePrism::Page
    section :header, Sections::HeaderSection, 'header'
  end
end

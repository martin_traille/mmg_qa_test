module Pages
  class ConfirmationPage < BasePage
    section :basketconfirmation, '#huc-v2-order-row-container' do
      element :basketprice, 'span.a-color-price.hlb-price.a-inline-block.a-text-bold'
      end
    #remember to camel case your section variables e.g addedToBasket
    element :addedtobasket, 'h1'

  end

end

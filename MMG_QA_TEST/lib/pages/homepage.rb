module Pages
  class AmazonHomepage < BasePage
    set_url '/'
    set_url_matcher %r{/}
  end
end
module Pages
class ProductPage < BasePage
  set_url_matcher %r{/i/\S+}

  element :title, '#productTitle' '.a-size-large'
  element :prodprice, '#priceblock_ourprice'
  element :addtobasket, '#add-to-cart-button'

end
end

module Pages
  class ResultsPage < BasePage
    set_url_matcher %r{/s/\S+}
    section :results, '#resultsCol' do
      sections :items, '.s-result-item' do
        element :image, '.s-item-container .a-spacing-base a'
        element :details, '.s-item-container .a-spacing-mini .s-access-detail-page'
        element :price, 'span.a-size-base.a-color-price.s-price.a-text-bold'
      end
    end
    section :refinements, '#refinements' do
      def select(refinement)
        element = first('.refinementLink', text: refinement)
        element.click
        wait_until do
          selected = first('.refinementLinkSelected', text: refinement)
          selected != nil
        end
      end

      def unselect(refinement)
        element = first('.refinementLinkSelected', text: refinement)
        element.click
        wait_until do
          unselected = first('.refinementLink', text: refinement)
          unselected != nil
        end
      end
    end
    element :sort, '.s-last-column'
  end
end
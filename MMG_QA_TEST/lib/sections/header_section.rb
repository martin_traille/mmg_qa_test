module Sections
  class HeaderSection < SitePrism::Section
    element :search_input, '.nav-searchbar .nav-search-field input'
  end
end
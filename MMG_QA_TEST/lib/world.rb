module GlobalSpace
  include ::Pages

  def homepage
    @current_page = AmazonHomepage.new
  end

  def results_page
    @current_page = ResultsPage.new
  end

  def product_page
    @current_page = ProductPage.new
  end

  def confirmation_page
    @current_page = ConfirmationPage.new
  end

end

World(GlobalSpace)
